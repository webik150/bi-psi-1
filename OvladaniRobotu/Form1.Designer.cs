﻿namespace OvladaniRobotu {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.startServerBtn = new System.Windows.Forms.Button();
			this.btStatus = new System.Windows.Forms.RichTextBox();
			this.robotsList = new System.Windows.Forms.ListView();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// startServerBtn
			// 
			this.startServerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.startServerBtn.Location = new System.Drawing.Point(3, 3);
			this.startServerBtn.Name = "startServerBtn";
			this.startServerBtn.Size = new System.Drawing.Size(133, 40);
			this.startServerBtn.TabIndex = 0;
			this.startServerBtn.Text = "Start Server";
			this.startServerBtn.UseVisualStyleBackColor = true;
			this.startServerBtn.Click += new System.EventHandler(this.button1_Click);
			// 
			// btStatus
			// 
			this.btStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.btStatus.Location = new System.Drawing.Point(3, 3);
			this.btStatus.Name = "btStatus";
			this.btStatus.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
			this.btStatus.Size = new System.Drawing.Size(651, 243);
			this.btStatus.TabIndex = 1;
			this.btStatus.Text = "";
			// 
			// robotsList
			// 
			this.robotsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			| System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));
			this.robotsList.LargeImageList = this.imageList1;
			this.robotsList.Location = new System.Drawing.Point(3, 252);
			this.robotsList.Name = "robotsList";
			this.robotsList.Size = new System.Drawing.Size(651, 195);
			this.robotsList.TabIndex = 2;
			this.robotsList.TileSize = new System.Drawing.Size(10, 10);
			this.robotsList.UseCompatibleStateImageBehavior = false;
			// 
			// imageList1
			// 
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			this.imageList1.Images.SetKeyName(0, "robot1.png");
			this.imageList1.Images.SetKeyName(1, "robot2.png");
			this.imageList1.Images.SetKeyName(2, "robot3.png");
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.robotsList);
			this.splitContainer1.Panel1.Controls.Add(this.btStatus);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.startServerBtn);
			this.splitContainer1.Size = new System.Drawing.Size(800, 450);
			this.splitContainer1.SplitterDistance = 657;
			this.splitContainer1.TabIndex = 3;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.splitContainer1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button startServerBtn;
		private System.Windows.Forms.RichTextBox btStatus;
		private System.Windows.Forms.ListView robotsList;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.SplitContainer splitContainer1;
	}
}

