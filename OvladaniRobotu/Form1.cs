﻿/*
 * made by jezekkr1 - Kryštof Ježek - 2019 
 * 
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace OvladaniRobotu {
	public partial class Form1 : Form {

		public ushort ServerKey => 54621;
		public ushort ClientKey => 45328;
		public string MoveForwardMessage => "102 MOVE" + EndingSequence;
		public string TurnLeftMessage => "103 TURN LEFT" + EndingSequence;
		public string TurnRightMessage => "104 TURN RIGHT" + EndingSequence;
		public string SearchMessage => "105 GET MESSAGE" + EndingSequence;
		public string LogoutMessage => "106 LOGOUT" + EndingSequence;
		public string ServerOkMessage => "200 OK" + EndingSequence;
		public string LoginFailedMessage => "300 LOGIN FAILED" + EndingSequence;
		public string SyntaxErrorMessage => "301 SYNTAX ERROR" + EndingSequence;
		public string LogicErrorMessage => "302 LOGIC ERROR" + EndingSequence;
		public string RechargingMessage => "RECHARGING";
		public string FullPowerMessage => "FULL POWER";
		public string EndingSequence => "\a\b";
		public TimeSpan Timeout => new TimeSpan(0, 0, 0, 0, 1000);
		public TimeSpan Rechargetimeout => new TimeSpan(0, 0, 0, 0, 5000);
		/// <summary>
		/// Regex for finding a movement confirmation.
		/// </summary>
		public Regex MoveConfirmRex = new Regex("^OK ([-0-9]+) ([-0-9]+)$");
		/// <summary>
		/// Regex for finding a command.
		/// </summary>
		public Regex CommandRex = new Regex("((?!\a\b).*?)\a\b|\a\b");
		/// <summary>
		/// Robot's state machine
		/// </summary>
		public enum RobotState {
			Unknown = 0,
			Authentifying = 1,
			Authentificated = 2,
			MoveInit1 = 3,
			MoveInit2 = 4,
			MovingToDestination = 5,
			Recharging = 6,
			FoundMesage = 7,
			WrongKey = 900,
		}
		/// <summary>
		/// Direction of a robot
		/// </summary>
		public enum Direction {
			Unknown,
			N,
			E,
			S,
			W
		}

		public class Robot {
			public int Id = -1;
			public string Name;
			public (int X, int Y) Position;
			public Direction Direction = Direction.Unknown;
			public (int X, int Y) Destination;
			public ListViewItem Item;
			/// <summary>
			/// Turns the robot left.
			/// </summary>
			public void TurnLeft() {
				switch (Direction) {
					case Direction.N:
						Direction = Direction.W;
						break;
					case Direction.E:
						Direction = Direction.N;
						break;
					case Direction.S:
						Direction = Direction.E;
						break;
					case Direction.W:
						Direction = Direction.S;
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			/// <summary>
			/// Turns the robot right (practically). Technically turns the robot left, then left and then left.
			/// </summary>
			public void TurnRight() {
				TurnLeft();
				TurnLeft();
				TurnLeft();
			}
			/// <summary>
			/// Tiles that can contain the hidden message
			/// </summary>
			private readonly List<(int X, int Y)> remainingTiles = new List<(int X, int Y)>
			{
				(-2,-2),(-1,-2),( 0,-2),( 1,-2),( 2,-2),
				(-2,-1),(-1,-1),( 0,-1),( 1,-1),( 2,-1),
				(-2, 0),(-1, 0),( 0, 0),( 1, 0),( 2, 0),
				(-2, 1),(-1, 1),( 0, 1),( 1, 1),( 2, 1),
				(-2, 2),(-1, 2),( 0, 2),( 1, 2),( 2, 2),
			};
			public RobotState State;
			public readonly ushort Checksum;
			/// <summary>
			/// Used in the UI
			/// </summary>
			public string Info => $"[{Id}]{Name}\n{State.ToString()}\n{Direction.ToString()}";
			public Robot(string name, ushort checksum) {
				Name = name;
				Checksum = checksum;

			}
			/// <summary>
			/// Gets the nearest coordinates
			/// </summary>
			public void GetDestination() {
				remainingTiles.Sort((tuple, valueTuple) => {
					int x1 = tuple.X - Position.X;
					int y1 = tuple.Y - Position.Y;
					int x2 = valueTuple.X - Position.X;
					int y2 = valueTuple.Y - Position.Y;

					double dist1 = Math.Sqrt(x1 * x1 + y1 * y1);
					double dist2 = Math.Sqrt(x2 * x2 + y2 * y2);

					if (Math.Abs(dist1 - dist2) < Double.Epsilon) return 0;
					else if (dist1 > dist2) return 1;
					else return -1;
				});
				if (remainingTiles.Count > 0) {
					Destination = remainingTiles[0];
					remainingTiles.Remove(Destination);
				} else {
					Destination = (666, 666);
				}

			}
		}

		public class StateObject {
			public System.Threading.Timer Timer;
			public Socket WorkSocket;
			public const int BUFFER_SIZE = 1024;
			public byte[] Buffer = new byte[BUFFER_SIZE];
			public StringBuilder Sb = new StringBuilder();
			public Robot Robot;
			public int Index;
			public int TempBytesRead;
			public AsyncCallback Callback;
			public RobotState LastState;
			public bool WasRecharging;
		}

		public static RichTextBox Log;
		private Socket listener;
		private bool serverOn;
		private Thread t;
		public static ManualResetEvent AllDone = new ManualResetEvent(false);

		public Form1() {
			InitializeComponent();
			Log = btStatus;
			// ReSharper disable once ObjectCreationAsStatement
			new SocketPermission(NetworkAccess.Accept, TransportType.Tcp, "", 3999);
		}
		/// <summary>
		/// Starts or stops the server.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private void button1_Click(object sender, EventArgs args) {
			if (serverOn) {
				serverOn = false;
				startServerBtn.Text = @"Start Server";
				if (listener.Connected) {
					listener.Disconnect(true);
				}
				listener.Close(1);
				listener = null;
				t.Abort();
				robotsList.Items.Clear();
			} else {
				serverOn = true;
				startServerBtn.Text = @"Stop Server";
				AddStatus("Starting server...", Color.DimGray);
				t = new Thread(StartServer);
				t.Start();
			}
		}
		/// <summary>
		/// Starts the server.
		/// </summary>
		public void StartServer() {
			IPAddress ipAddr = IPAddress.Any;
			var ipEndPoint = new IPEndPoint(ipAddr, 3999);

			listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			try {
				listener.Bind(ipEndPoint);
				listener.Listen(10);
				AddStatus("Server Started! Listening on " + ipAddr.MapToIPv4() + ":" + ipEndPoint.Port, Color.DimGray);
				while (serverOn) {
					AllDone.Reset();
					AddStatus("Waiting for a connection...", Color.DarkGray);
					listener.BeginAccept(
						AcceptCallback,
						listener);
					AllDone.WaitOne();
				}

			} catch (ThreadAbortException) {
				AddStatus("Server stopped.", Color.OrangeRed);
			} catch (Exception e) {
				AddStatus(e.ToString(), Color.Red);
				Console.WriteLine(e.ToString());
			}
		}
		/// <summary>
		/// Starts charging the robot
		/// </summary>
		/// <param name="state"></param>
		/// <param name="callback"></param>
		private void StartCharging(StateObject state, AsyncCallback callback) {
			Socket handler = state.WorkSocket;
			AddStatus("Started recharging", Color.Coral);
			state.Index += RechargingMessage.Length + 2;
			state.LastState = state.Robot.State;
			state.Robot.State = RobotState.Recharging;
			UpdateRobot(state.Robot);
			state.Timer?.Dispose();
			state.Callback = callback;
			state.Timer =
				new System.Threading.Timer(obj => { SendTimeout(state); }, state, Rechargetimeout, System.Threading.Timeout.InfiniteTimeSpan);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, GetRechargeResult, state);
		}
		/// <summary>
		/// Calculates robot's checksum
		/// </summary>
		/// <param name="stateBuffer"></param>
		/// <param name="bytesRead"></param>
		/// <returns></returns>
		private ushort CalculateHash(byte[] stateBuffer, int bytesRead) {
			int temp = 0;
			for (int i = 0; i < bytesRead; i++) {
				temp += stateBuffer[i];
			}

			return (ushort)(((temp * 1000) % 65536));
		}
		/// <summary>
		/// Sends packets to the robot
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="state"></param>
		private void Send(Socket handler, StateObject state) {
			byte[] byteData;
			switch (state.Robot.State) {
				case RobotState.Unknown:
					AddStatus("ERROR: WRONG STATE", Color.Red);
					return;
				case RobotState.Authentifying:
					byteData = Encoding.ASCII.GetBytes((state.Robot.Checksum + ServerKey) % 65536 + EndingSequence);
					break;
				case RobotState.Authentificated:
					byteData = Encoding.ASCII.GetBytes(ServerOkMessage);
					break;
				case RobotState.WrongKey:
					byteData = Encoding.ASCII.GetBytes(LoginFailedMessage);
					break;
				case RobotState.MoveInit1:
					byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
					break;
				case RobotState.MoveInit2:
					byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
					break;
				case RobotState.MovingToDestination:
					byteData = new[] { new byte() };
					break;
				case RobotState.FoundMesage:
					byteData = Encoding.ASCII.GetBytes(LogoutMessage);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			handler.BeginSend(byteData, 0, byteData.Length, 0,
				SendCallback, state);
		}

		#region Errors
		/// <summary>
		/// Sends the syntax error message.
		/// </summary>
		/// <param name="handler"></param>
		private void SendSyntaxError(Socket handler) {
			byte[] byteData = Encoding.ASCII.GetBytes(SyntaxErrorMessage);
			AddStatus("SYNTAX ERROR", Color.Red);
			handler.Send(byteData);
			handler.Shutdown(SocketShutdown.Both);
			handler.Close();
		}
		/// <summary>
		/// Sends the logic error message.
		/// </summary>
		/// <param name="handler"></param>
		private void SendLogicError(Socket handler) {
			byte[] byteData = Encoding.ASCII.GetBytes(LogicErrorMessage);
			AddStatus("LOGIC ERROR", Color.Red);
			handler.Send(byteData);
			handler.Shutdown(SocketShutdown.Both);
			handler.Close();
		}
		/// <summary>
		/// Disconnects because of a timeout.
		/// </summary>
		/// <param name="state"></param>
		private void SendTimeout(StateObject state) {
			try {
				AddStatus("Timed out");
				if (state.Robot != null) RemoveRobot(state.Robot);
				state.WorkSocket.Shutdown(SocketShutdown.Both);
				state.WorkSocket.Close();
			} catch (ObjectDisposedException) { }
		}

		#endregion

		#region UIAndDebug
		/// <summary>
		/// Adds a robot to the UI
		/// </summary>
		/// <param name="stateRobot"></param>
		private void AddRobot(Robot stateRobot) {
			stateRobot.Id = robotsList.Items.Count;
			robotsList.Invoke((MethodInvoker)(() => { stateRobot.Item = robotsList.Items.Add(stateRobot.Info, new Random().Next(3)); }));
		}
		/// <summary>
		/// Updates robot's text in the UI
		/// </summary>
		/// <param name="stateRobot"></param>
		private void UpdateRobot(Robot stateRobot) {
			robotsList.Invoke((MethodInvoker)(() => { robotsList.Items[stateRobot.Id].Text = stateRobot.Info; }));
		}
		/// <summary>
		/// Removes a robot from the UI
		/// </summary>
		/// <param name="stateRobot"></param>
		private void RemoveRobot(Robot stateRobot) {
			if (stateRobot == null) return;
			robotsList.Invoke((MethodInvoker)(() => {
				if (robotsList.Items.Count > stateRobot.Id)
					robotsList.Items.RemoveAt(stateRobot.Id);
			}));
		}
		/// <summary>
		/// Sets the robot's text to some message
		/// </summary>
		/// <param name="stateRobot"></param>
		/// <param name="message"></param>
		private void SetTooltip(Robot stateRobot, string message) {
			if (stateRobot == null) return;
			robotsList.Invoke((MethodInvoker)(() => {
				robotsList.Items[robotsList.Items.IndexOf(stateRobot.Item)].Text = message.Replace("\a\b", "");

			}));
		}
		/// <summary>
		/// Adds a line to the log.
		/// </summary>
		/// <param name="status"></param>
		/// <param name="newline"></param>
		public void AddStatus(string status, bool newline = true) {
			btStatus.Invoke((MethodInvoker)(() => {
				btStatus.AppendText(status + (newline ? Environment.NewLine : ""));
			}));
		}
		/// <summary>
		/// Adds a colored line to the log.
		/// </summary>
		/// <param name="status"></param>
		/// <param name="c"></param>
		/// <param name="newline"></param>
		public void AddStatus(string status, Color c, bool newline = true) {
			btStatus.Invoke((MethodInvoker)(() => {
				btStatus.SelectionStart = btStatus.TextLength;
				btStatus.SelectionLength = 0;
				btStatus.SelectionColor = c;
				btStatus.AppendText(status + (newline ? Environment.NewLine : ""));
				btStatus.SelectionColor = btStatus.ForeColor;
			}));
		}

		#endregion

		#region Movement
		/// <summary>
		/// Moves the robot towards destination.
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="state"></param>
		public void MoveToDestination(Socket handler, StateObject state) {

			Robot bot = state.Robot;
			byte[] byteData;
			if (bot.Destination.Equals((666, 666))) {
				handler.Shutdown(SocketShutdown.Both);
				handler.Close();
				return;
			}
			if (Equals(bot.Destination, bot.Position)) {
				byteData = Encoding.ASCII.GetBytes(SearchMessage);
				handler.BeginSend(byteData, 0, byteData.Length, 0, Dig, state);
			} else {
				if (bot.Position.X == bot.Destination.X) {  //Target is south or north
					if (bot.Position.Y > bot.Destination.Y) { //Target is south
						if (bot.Direction == Direction.S) {
							byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
							handler.BeginSend(byteData, 0, byteData.Length, 0, MoveForward, state);
						} else {
							if (bot.Direction == Direction.E) {
								bot.TurnRight();
								byteData = Encoding.ASCII.GetBytes(TurnRightMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnRight, state);
							} else {
								bot.TurnLeft();
								byteData = Encoding.ASCII.GetBytes(TurnLeftMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnLeft, state);
							}
						}
					} else {                                 //Target is north
						if (bot.Direction == Direction.N) {
							byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
							handler.BeginSend(byteData, 0, byteData.Length, 0, MoveForward, state);
						} else {
							if (bot.Direction == Direction.W) {
								bot.TurnRight();
								byteData = Encoding.ASCII.GetBytes(TurnRightMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnRight, state);
							} else {
								bot.TurnLeft();
								byteData = Encoding.ASCII.GetBytes(TurnLeftMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnLeft, state);
							}
						}
					}
				} else                                      //Target is east or west
				  {
					if (bot.Position.X < bot.Destination.X) {
						//Target is east
						switch (bot.Direction) {
							case Direction.E:
								byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, MoveForward, state);
								break;
							case Direction.N:
								bot.TurnRight();
								byteData = Encoding.ASCII.GetBytes(TurnRightMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnRight, state);
								break;
							default:
								bot.TurnLeft();
								byteData = Encoding.ASCII.GetBytes(TurnLeftMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnLeft, state);
								break;
						}
					} else {
						//Target is west
						switch (bot.Direction) {
							case Direction.W:
								byteData = Encoding.ASCII.GetBytes(MoveForwardMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, MoveForward, state);
								break;
							case Direction.S:
								bot.TurnRight();
								byteData = Encoding.ASCII.GetBytes(TurnRightMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnRight, state);
								break;
							default:
								bot.TurnLeft();
								byteData = Encoding.ASCII.GetBytes(TurnLeftMessage);
								handler.BeginSend(byteData, 0, byteData.Length, 0, TurnLeft, state);
								break;
						}
					}
				}
			}
		}
		/// <summary>
		/// Starts checking for hidden message.
		/// </summary>
		/// <param name="ar"></param>
		public void Dig(IAsyncResult ar) {

			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
				GetDigResult, state);
		}
		/// <summary>
		/// Starts checking for movement confirmation message.
		/// </summary>
		/// <param name="ar"></param>
		public void TurnLeft(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
				MoveForwardConfirm, state);
		}
		/// <summary>
		/// Starts checking for movement confirmation message.
		/// </summary>
		/// <param name="ar"></param>
		public void TurnRight(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
				MoveForwardConfirm, state);
		}
		/// <summary>
		/// Starts checking for movement confirmation message.
		/// </summary>
		/// <param name="ar"></param>
		public void MoveForward(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
				MoveForwardConfirm, state);
		}

		#endregion

		#region AsyncCallbacks
		/// <summary>
		/// Begings checking for packets after message has been sent or disconnects the robot if the message sent was logout or login error.
		/// </summary>
		/// <param name="ar"></param>
		private void SendCallback(IAsyncResult ar) {
			try {
				var state = (StateObject)ar.AsyncState;
				Socket handler = state.WorkSocket;
				int bytesSent = handler.EndSend(ar);
				AddStatus($"Sent {bytesSent} bytes to client.", Color.ForestGreen);
				if (state.Robot.State == RobotState.Authentificated) {
					state.Robot.State = RobotState.MoveInit1;
					Send(handler, state);
				} else if (state.Robot.State == RobotState.FoundMesage || state.Robot.State == RobotState.WrongKey) {
					if (state.Robot != null && state.Robot.State != RobotState.FoundMesage) RemoveRobot(state.Robot);
					handler.Shutdown(SocketShutdown.Both);
					handler.Close();
				} else {
					state.Timer?.Dispose();
					state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
					handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
						ReadCallback, state);
				}
			} catch (Exception e) {
				AddStatus(e.ToString(), Color.Red);
			}
		}
		/// <summary>
		/// Creates the initial StateObject and begins listening.
		/// </summary>
		/// <param name="ar"></param>
		public void AcceptCallback(IAsyncResult ar) {
			try {
				AllDone.Set();
				Socket handler = ((Socket)ar.AsyncState).EndAccept(ar);
				handler.ReceiveTimeout = 10000;
				handler.SendTimeout = 10000;
				var state = new StateObject { WorkSocket = handler };
				state.Timer?.Dispose();
				state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, 1000, System.Threading.Timeout.Infinite);
				handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
					ReadCallback, state);
			} catch (ObjectDisposedException) {

			}
		}
		/// <summary>
		/// Helper method that properly recieves robot's auth code in case recharging started before it was sent.
		/// </summary>
		/// <param name="ar"></param>
		public void WaitForCallback(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			state.WasRecharging = false;
			state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, 1000, System.Threading.Timeout.Infinite);
			handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0,
				ReadCallback, state);
		}
		/// <summary>
		/// Provides logic for authentification and initialization of robot's position and direction.
		/// </summary>
		/// <param name="ar"></param>
		public void ReadCallback(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			int bytesRead;
			string content;
			try {
				if (!state.WasRecharging) {
					bytesRead = handler.EndReceive(ar);
				} else {
					content = state.Sb.ToString();
					bytesRead = content.Substring(state.Index).Length;
				}
			} catch (ObjectDisposedException) {
				if (state.Robot != null) RemoveRobot(state.Robot);
				return;
			}
			state.TempBytesRead += bytesRead;
			if (!state.WasRecharging) {
				state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

			} else {
				state.WasRecharging = false;
			} 
			content = state.Sb.ToString();
			if (content.Substring(state.Index).EndsWith("\a\b") || content.Substring(state.Index).Contains("\a\b")) {
				state.TempBytesRead = 0;
				var messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
				if (state.Robot == null) {
					if (messages[0].Length > 10) {
						SendSyntaxError(handler);
						return;
					}
					ushort confirmationMessage = CalculateHash(Encoding.ASCII.GetBytes(messages[0]), messages[0].Length);
					state.Robot = new Robot(messages[0], confirmationMessage);
					AddRobot(state.Robot);
					state.Robot.State = RobotState.Authentifying;
					UpdateRobot(state.Robot);
					state.Index += messages[0].Length + 2;
					if (messages.Count > 1) {
						handler.Send(Encoding.ASCII.GetBytes((state.Robot.Checksum + ServerKey) % 65536 + EndingSequence));
					} else {
						Send(handler, state);
						return;
					}

				}
				switch (state.Robot.State) {
					case RobotState.Unknown:
						break;
					case RobotState.Authentifying:
						if (messages.Last().Equals(RechargingMessage)) {
							StartCharging(state, WaitForCallback);
							return;
						}
						messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
						if (messages[0].Length > 5 || !messages[0].All(char.IsDigit)) {
							AddStatus("Wrong length! Expected max 7, got " + messages[0].Length, Color.Red);
							RemoveRobot(state.Robot);
							SendSyntaxError(handler);
							return;
						}
						ushort robotKey = (ushort)((ushort.Parse(messages[0]) - ClientKey) % 65536);
						state.Robot.State = robotKey == state.Robot.Checksum ? RobotState.Authentificated : RobotState.WrongKey;
						UpdateRobot(state.Robot);
						state.Index += messages[0].Length + 2;
						if (messages.Count > 1 && state.Robot.State == RobotState.Authentificated) {
							handler.Send(Encoding.ASCII.GetBytes(ServerOkMessage));
							handler.Send(Encoding.ASCII.GetBytes(MoveForwardMessage));
							state.Robot.State = RobotState.MoveInit1;
							goto case RobotState.MoveInit1;
						} else {
							Send(handler, state);
						}
						break;
					case RobotState.Authentificated:
						break;
					case RobotState.WrongKey:
						handler.Shutdown(SocketShutdown.Both);
						handler.Close();
						return;
					case RobotState.MoveInit1:
						if (messages.Last().Equals(RechargingMessage)) {
							StartCharging(state, ReadCallback);
							return;
						}
						messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
						Match match = MoveConfirmRex.Match(messages[0]);
						if (!match.Success || messages[0].Length > 10) {
							AddStatus("Wrong syntax! Expected max 12, got " + messages[0].Length, Color.Red);
							RemoveRobot(state.Robot);
							SendSyntaxError(handler);
							return;
						}
						state.Robot.Position = (int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value));
						AddStatus("Initial coordinates at ", false);
						AddStatus(state.Robot.Position.X + "," + state.Robot.Position.Y, Color.Green);
						state.Robot.State = RobotState.MoveInit2;
						UpdateRobot(state.Robot);
						state.Index += messages[0].Length + 2;
						if (messages.Count > 1) {
							handler.Send(Encoding.ASCII.GetBytes(MoveForwardMessage));
							state.Robot.State = RobotState.MoveInit2;
							goto case RobotState.MoveInit2;
						} else {
							Send(handler, state);
						}
						break;
					case RobotState.MoveInit2:
						if (messages.Last().Equals(RechargingMessage)) {
							StartCharging(state, ReadCallback);
							return;
						}
						messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
						match = MoveConfirmRex.Match(messages[0]);
						if (!match.Success || messages[0].Length > 10) {
							AddStatus("Wrong syntax! Expected max 12, got " + messages[0].Length, Color.Red);
							RemoveRobot(state.Robot);
							SendSyntaxError(handler);
							return;
						}

						(int X, int Y) oldPos = state.Robot.Position;
						state.Robot.Position = (int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value));
						(int X, int Y) dir = (state.Robot.Position.X - oldPos.X, state.Robot.Position.Y - oldPos.Y);
						switch (dir.X) {
							case -1:
								state.Robot.Direction = Direction.W;
								break;
							case 1:
								state.Robot.Direction = Direction.E;
								break;
							case 0:
								state.Robot.Direction = dir.Y == -1 ? Direction.S : Direction.N;
								break;
						}
						AddStatus("Confirming coordinates. Robot is facing ", false);
						AddStatus(state.Robot.Direction.ToString(), Color.LimeGreen);
						state.Robot.GetDestination();
						state.Robot.State = RobotState.MovingToDestination;
						state.Index += messages[0].Length + 2;
						UpdateRobot(state.Robot);
						MoveToDestination(handler, state);
						return;
					default:
						throw new ArgumentOutOfRangeException();
				}
			} else {
				if (bytesRead > 11) {
					if (state.Robot != null) RemoveRobot(state.Robot);
					SendSyntaxError(handler);
					return;
				}

				state.Timer?.Dispose();
				state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
				handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, ReadCallback, state);
			}
		}
		/// <summary>
		/// Checks if robot found the hidden message.
		/// </summary>
		/// <param name="ar"></param>
		public void GetDigResult(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			state.Timer?.Dispose();
			Socket handler = state.WorkSocket;
			int bytesRead;
			string content;
			try {
				if (!state.WasRecharging) {
					bytesRead = handler.EndReceive(ar);
				} else {
					content = state.Sb.ToString();
					bytesRead = content.Substring(state.Index).Length;
				}
			} catch (ObjectDisposedException) {
				if (state.Robot != null) RemoveRobot(state.Robot);
				return;
			}
			state.TempBytesRead += bytesRead;
			if (!state.WasRecharging) {
				state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));
			} else {
				state.WasRecharging = false;
			}
			content = state.Sb.ToString();
			if (bytesRead > 0 && content.IndexOf("\a\b", state.Index, StringComparison.Ordinal) > -1) {
				if (content.Substring(state.Index).Equals(RechargingMessage + EndingSequence)) {
					StartCharging(state, GetDigResult);
					return;
				}
				bytesRead = state.TempBytesRead;
				state.TempBytesRead = 0;
				if (content.Substring(state.Index).Equals("\a\b")) {
					AddStatus("Message not at ", false); AddStatus(state.Robot.Position.ToString(), Color.Gray, false); AddStatus(". Resuming search.");
					state.Robot.GetDestination();
					AddStatus("New destination is ", false); AddStatus(state.Robot.Destination.ToString(), Color.OrangeRed);
					state.Index += bytesRead;
					UpdateRobot(state.Robot);
					MoveToDestination(handler, state);
					return;
				}

				AddStatus("MESSAGE FOUND:  ", Color.DeepPink, false);
				AddStatus(content.Substring(state.Index), Color.Green);
				UpdateRobot(state.Robot);
				state.Robot.State = RobotState.FoundMesage;
				SetTooltip(state.Robot, content.Substring(state.Index));
				state.Index += bytesRead;
				Send(handler, state);
			} else {
				if (bytesRead > 99) {
					if (state.Robot != null) RemoveRobot(state.Robot);
					SendSyntaxError(handler);
					return;
				}

				state.Timer?.Dispose();
				state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
				handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, GetDigResult, state);
			}
		}
		/// <summary>
		/// Checks the movement confirmation message.
		/// </summary>
		/// <param name="ar"></param>
		public void MoveForwardConfirm(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			Socket handler = state.WorkSocket;
			state.Timer?.Dispose();
			string content;
			int bytesRead;
			try {
				if (!state.WasRecharging) {
					bytesRead = handler.EndReceive(ar);
				} else {
					content = state.Sb.ToString();
					bytesRead = content.Substring(state.Index).Length;
				}
			} catch (ObjectDisposedException) {
				if (state.Robot != null) RemoveRobot(state.Robot);
				return;
			}
			state.TempBytesRead += bytesRead;
			if (!state.WasRecharging) {
				state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));
			} else {
				state.WasRecharging = false;
			}
			content = state.Sb.ToString();
			if (bytesRead > 0 && content.IndexOf("\a\b", state.Index, StringComparison.Ordinal) > -1) {
				var messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
				state.TempBytesRead = 0;
				Match match = MoveConfirmRex.Match(messages[0]);
				if (!match.Success) {
					if (messages[0].Equals(RechargingMessage)) {
						StartCharging(state, MoveForwardConfirm);
						return;
					}
					if (state.Robot != null) RemoveRobot(state.Robot);
					SendSyntaxError(handler);
					return;

				}
				state.Robot.Position = (int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value));
				AddStatus("Moved to ", false);
				AddStatus(state.Robot.Position.X + "," + state.Robot.Position.Y, Color.Green);
				UpdateRobot(state.Robot);
				state.Index += messages[0].Length + 2;
				MoveToDestination(handler, state);
			} else {
				state.Timer?.Dispose();
				state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
				handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, MoveForwardConfirm, state);
			}
		}
		/// <summary>
		/// Waits for recharge.
		/// </summary>
		/// <param name="ar"></param>
		private void GetRechargeResult(IAsyncResult ar) {
			var state = (StateObject)ar.AsyncState;
			state.Timer?.Dispose();
			Socket handler = state.WorkSocket;
			int bytesRead;
			try {
				bytesRead = handler.EndReceive(ar);
			} catch (ObjectDisposedException) {
				if (state.Robot != null) RemoveRobot(state.Robot);
				return;
			}
			state.TempBytesRead += bytesRead;
			if (bytesRead > 0) {
				state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));
				string content = state.Sb.ToString();
				if (content.IndexOf("\a\b", state.Index, StringComparison.Ordinal) > -1) {
					state.TempBytesRead = 0;
					var messages = CommandRex.Matches(content.Substring(state.Index)).Cast<Match>().SelectMany(x => x.Groups.Cast<Group>().Skip(1).Select(y => y.Value)).Where(z => !string.IsNullOrEmpty(z)).ToList();
					if (messages[0].Contains(FullPowerMessage)) {
						state.Index += FullPowerMessage.Length + 2;
						state.Robot.State = state.LastState;
						state.WasRecharging = true;
						UpdateRobot(state.Robot);
						state.Callback.DynamicInvoke(ar);
						return;
					}
					RemoveRobot(state.Robot);
					SendLogicError(handler);
				} else {
					if (bytesRead > 99) {
						if (state.Robot != null) RemoveRobot(state.Robot);
						SendSyntaxError(handler);
						return;
					}
					state.Timer?.Dispose();
					state.Timer = new System.Threading.Timer(obj => { SendTimeout(state); }, state, Timeout, System.Threading.Timeout.InfiniteTimeSpan);
					handler.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, GetRechargeResult, state);
				}
			}
		}

		#endregion
	}
}
